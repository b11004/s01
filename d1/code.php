<?php 

// PHP Comments
// This is a single-line comment
/*
	This is a multi-line comment
*/

//============================

// Variables - Used to store values or contain data
// Variable in PHP are defined using the dollar ($) notation before the name of the variable.

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

//=============================

// Constants
// Constants are defined usiing the define() function.

// Constant variable
define('PI', 3.1416);

//==============================

// DATA TYPES

// String
$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country; // Concat via dot notation
$addressTwo = "$state, $country"; // Concat via double quotes

// Integer
$age = 31;
$headcount = 26;

// FLoat
$grade = 98.2;
$distanceInKm = 2562.23;

// Boolean
$hastravelledAbroad = false;
$hasSymptoms = true;

// Null
$spouse = null;
$middleName = null;

// Array
$grades = array(98.7, 92.1, 90.2, 94.6);

// Objects 
$gradesObj = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 94.6,
	'fourthGrading' => 90.2
];

$personObj = (object) [
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'Washington DC',
		'country' => 'United States of America'
	]
];

//============================

// OPERATORS

// Assignment Operators
$x = 56.2;
$y = 912.6;

$isLegalAge = true;
$isRegistered = false;

//============================

// FUNCTIONS

function getFullName($firstName, $middleInitial, $lastName) {
	return "$lastName, $firstName $middleInitial.";
}

//============================

// SELECTION CONTROL STRUCTURE

// if-elseif-else statement

function determineTyphoonIntensity($windSpeed) {
	if($windSpeed < 30) {
		return 'Not a typhoon yet';
	}
	else if($windSpeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if($windSpeed >=61 &&  $windSpeed <= 88) {
		return 'Tropical Storm detected';
	}
	else if ($windSpeed >=82 && $windSpeed <= 177) {
		return 'Severe Tropical storm detected';
	}
	else {
		return 'typhoon detected';
	}
}


// Conditional (Ternary) Operator
function isUnderAge($age){
	return ($age < 18) ? true : false;
}

// Switch Statement
function determineComputerUser($computerNumber) {
	switch($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2: 
			return 'Steve Jobs';
			break;
		case 3: 
			return 'Sid Meier';
			break;
		case 4: 
			return 'Allbert Einstein';
			break;
		case 5: 
			return 'Charles Babbage';
			break;
		default:
			return $computerNummber.' is out of bounds.';
	}
}


// Try-Catch-Finally
function greeting($str) {
	try {
		if(gettype($str) == "string") {
			echo $str;
		}
		else {
			throw new Exception("Opps!");
		}
	}
	// Container of else
	catch(Exception $e) {
		echo $e->getMessage();
	}
	finally {
		echo " I did it again!";
	}
}