<?php

function getFullAddress($lotNo, $building, $street, $city, $province, $country) {
	return "$lotNo, $building, $street, $city, $province, $country";
}



function getLetterGrade($letter) {
	if($letter < 75) {
		return 'F';
	}
	else if($letter >= 75 && $letter <= 76) {
		return 'C-';
	}
	else if($letter >= 77 && $letter <= 79) {
		return 'C';
	}
	else if($letter >= 80 && $letter <= 82) {
		return 'C+';
	}
	else if($letter >= 83 && $letter <= 85) {
		return 'B-';
	}
	else if($letter >= 86 && $letter <= 88) {
		return 'B';
	}
	else if($letter >= 89 && $letter <= 91) {
		return 'B+';
	}
	else if($letter >= 92 && $letter <= 94) {
		return 'A-';
	}
	else if($letter >= 95 && $letter <= 97) {
		return 'A';
	}
	else if($letter >= 98 && $letter <= 100) {
		return 'A+';
	}
	else {
		return 'No grade detected!';
	}
}